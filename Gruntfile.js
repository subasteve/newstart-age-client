module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    usebanner: {
    	doc: {
      		options: {
        		position: 'top',
        		banner: "<?php include_once '/var/www/jabber.newstartrs3.com/htdocs/Auth.php';"+
                        "$auth = new Auth;"+
                        "if(!$auth->success()){"+
                                "http_response_code(401);"+
                                "return;"+
                        "}?>",
        		linebreak: true
      		},
      		files: {
       			 src: [ 'doc/**/*.php' ]
      		}
    	}
    },
    groc: {
	javascript: [
		"package/js/**/*.js", "README.md"
	],
	options: {
		"out": "build/doc/"
	}
    },
    phplint: {
	options: {
		swapPath: '/tmp'
	},
	all: ['api/*.php']
    },
    phpcs: {
    	application: {
    	    dir: ['api/*.php']
    	},
    	options: {
    	    bin: 'vendor/bin/phpcs',
    	    standard: 'Zend'
    	}
    },
    php_analyzer: {
	options: {
            bin: 'vendor/bin/phpalizer',
    	},
        application: {
	    dir: 'api',
	    command: 'run'
	}
    },
    base64Less: {
    	fonts: {
		prefix: "font-",
		process: ['package/fonts/*','bowerBuild/fonts/*'],
		dest: 'package/less/fonts.less'
	}
    },
    'closure-compiler': {
	bower1: {
      		closurePath: './closure-compiler',
      		js: ['bowerBuild/js/*/*/*.js'],
      		jsOutputPath: './bowerBuild/jsCompiled/base/base/',
      		maxBuffer: 1024,
      		options: {
        		//compilation_level: 'ADVANCED_OPTIMIZATIONS',
        		language_in: 'ECMASCRIPT5'
      		}
    	},
	bower2: {
                closurePath: './closure-compiler',
                js: ['bowerBuild/js/*/*.js'],
                jsOutputPath: './bowerBuild/jsCompiled/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	bower3: {
                closurePath: './closure-compiler',
                js: ['bowerBuild/js/*.js'],
                jsOutputPath: './bowerBuild/jsCompiled/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app1: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*/*/*/*/*/*.js'],
                jsOutputPath: './bowerBuild/jsCompiled/base/base/base/base/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app2: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*/*/*/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/base/base/base/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app3: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*/*/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/base/base/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app4: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/base/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app5: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
	app6: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
        app7: {
                closurePath: './closure-compiler',
                js: ['package/js/*/*.js'],
                jsOutputPath: './build/jsCompiled/base/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        },
        app8: {
                closurePath: './closure-compiler',
                js: ['package/js/*.js'],
                jsOutputPath: './build/jsCompiled/',
                maxBuffer: 1024,
                options: {
                        //compilation_level: 'ADVANCED_OPTIMIZATIONS',
                        language_in: 'ECMASCRIPT5'
                }
        }
    },
    rename: {
	fixDep: {
		src: 'bowerBuild/js/jquery.js',
		dest: 'bowerBuild/js/base/base/'
	},
	fixDep2:{
		src: 'bowerBuild/js/underscore.js',
                dest: 'bowerBuild/js/base/base/'
	},
	fixDep3:{
		src: 'bowerBuild/js/knockout.js',
                dest: 'bowerBuild/js/base/'
	},
	fixDep4:{
                src: 'bowerBuild/js/backbone.js',
                dest: 'bowerBuild/js/base/'
        }
    },
    mkdir: {
    	all: {
      		options: {
        		mode: 0700,
        		create: ['bowerBuild','build','build/js','bowerBuild/js/base','bowerBuild/js/base/base','bowerBuild/jsCompiled']
      		}
    	}
    },
    htmlmin: {                                     // Task
    	dev: {                                      // Target
		options: {                                 // Target options
			removeComments: true,
			collapseWhitespace: true
		},
		files: {                                   // Dictionary of files
        		'build/index.php': 'package/index.php',     // 'destination': 'source'
        		'build/404.html': 'package/404.html'
		}
	}
    },
    clean: {
	doc: ["doc"],
    	git: ["bowerBuild","bower_components","build","dist","node_modules","packages","*.lock"],
	dev: ["build","dist"],
	dist: ["build","dist","bowerBuild"],
	build: ["jsCompiled"],
	bowerBuildUnneeded: ["bowerBuild/css/font-awesome.css","bowerBuild/css/bootstrap.css","bowerBuild/less/bootstrap.less"]
    },
    concat: {
	js: {
		src: ['bowerBuild/jsCompiled/*/*/*.js','bowerBuild/jsCompiled/*/*.js','bowerBuild/jsCompiled/*.js'],
		dest: 'build/js/depends.js'
	},
        bowerUncompiled: {
		src: ['bowerBuild/js/*/*/*.js','bowerBuild/js/*/*.js','bowerBuild/js/*.js'],
                dest: 'build/js/depends.js'
	},
  	css: {
    		src: ['bowerBuild/css/*.css', 'build/css/index.css'],
    		dest: 'build/css/index.css'
  	},
	app: {
		options: {
			banner: 'var NewstartAge = {};'
		},
		src: [
		'package/js/*/*/*/*/*/*/*/*.js',
                'package/js/*/*/*/*/*/*/*.js',
                'package/js/*/*/*/*/*/*.js',
                'package/js/*/*/*/*/*.js',
                'package/js/*/*/*/*.js',
                'package/js/*/*/*.js',
                'package/js/*/*.js',
                'package/js/*.js'
		],
		dest: 'build/js/index.js'
	},
	appCompiled: {
                options: {
			banner: 'var NewstartAge = {};'
		},
        	src: [
		'build/jsCompiled/*/*/*/*/*/*/*/*.js',
		'build/jsCompiled/*/*/*/*/*/*/*.js',
		'build/jsCompiled/*/*/*/*/*/*.js',
		'build/jsCompiled/*/*/*/*/*.js',
		'build/jsCompiled/*/*/*/*.js',
		'build/jsCompiled/*/*/*.js',
		'build/jsCompiled/*/*.js',
		'build/jsCompiled/*.js'
		],
                dest: 'build/js/index.js'
        },
	debug: {
		src: ['package/index.php'],
		dest: 'build/index.php'
	},
	debugLocal: {
		src: ['package/index.html'],
		dest: 'build/index.html'
	}
    },
    copy: {
	doc: {
		files: [{
			expand: true,
			dot: true,
			cwd: 'build/doc',
			dest: 'doc/',
			src: [
				'**/{,*/}*.html'
			],
			rename: function(dest, src) {
				return dest + src.replace('.html','.php');
			}
		},
	    	{expand: true, cwd: 'build/doc/assets', src: ['**'], dest: './doc/assets'}
	    ]
	},
    	main: {
		files: [
			// includes files within path
			{expand: true, cwd: 'bower_components/font-awesome/fonts/', src: ['**'], dest: 'bowerBuild/fonts/'},
			{expand: true, cwd: 'bower_components/bootstrap/less/', src: ['**'], dest: 'bowerBuild/less/bootstrap'},
			{expand: true, cwd: 'bower_components/font-awesome/less/', src: ['**'], dest: 'bowerBuild/less/font-awesome'}
		]
  	},
  	deploy: {
  		files: [
			{expand: true, cwd: 'dist/', src: ['**'], dest: './'}
		]
	},
	deployBuild: {
                files: [
                        {expand: true, cwd: 'build/', src: ['**'], dest: './'}
                ]
        },
	site: {
		files: [
			{expand: true, cwd: 'package/', src: ['index.php','404.html','robots.txt','favicon.ico'], dest: 'build/'}
		]
	},
	siteLocal: {
		files: [
			{expand: true, cwd: 'package/', src: ['index.html','404.html','robots.txt','favicon.ico'], dest: 'build/'}
		]
	}
    },
    bower: {
  	dev: {
    		dest: 'bowerBuild/',
		js_dest: 'bowerBuild/js',
    		css_dest: 'bowerBuild/css',
		oft_dest: 'bowerBuild/fonts',
		svg_dest: 'bowerBuild/fonts',
		wo_dest: 'bowerBuild/fonts',
		eot_dest: 'bowerBuild/fonts',
		ttf_dest: 'bowerBuild/fonts',
		woff_dest: 'bowerBuild/fonts',
		less_dest: 'bowerBuild/less'
	}
    },
    'cssmin': {
    	'dist': {
        	'src': ['build/css/index.css'],
        	'dest': 'build/css/index.css'
    	}
    },
    less: {
    	development: {
		options: {
			paths: ["css"]
		},
		files: {
			"build/css/index.css": "package/less/style.less"
		}
	}
    },
    compress: {
  	main: {
    		options: {
      			mode: 'gzip',
			level: 9
    		},
		expand: true,
		cwd: 'build/',
    		src: ['**/*'],
    		dest: 'dist/'
	}
   },
   imagemin: {
	dynamic: {                         // Another target
		files: [{
			expand: true,                  // Enable dynamic expansion
			cwd: 'package/img/',                   // Src matches are relative to this path
			src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
			dest: 'build/img'                  // Destination path prefix
      		}]
    	}
   },
   jshint: {
   	gruntfile: {
   		options: {
   			jshintrc: '.jshintrc'
   		},
   		src: 'Gruntfile.js'
   	},
   	src: {
   		options: {
   			jshintrc: '.jshintrc',
   			ignores: ['package/js/util/json/binary/msgpack.js','package/js/io/websocket/WebSocket.js','package/js/util/json/binary/Bson.js','package/js/util/encryption/md5.js','package/js/util/sheetengine-src-1.2.0.js','package/js/util/SheetEngine-3.0.js','package/js/util/Gamepad.js']
   		},
   		src: ['package/js/**/*.js']
	},
	test: {
		options: {
			jshintrc: '.jshintrc'
		},
		src: ['test/**/*.js']
	}
   },
   replace: {
   	fontAwesomeInline: {
		src: ['bowerBuild/less/font-awesome/path.less'],
		dest: 'bowerBuild/less/font-awesome/path.less',
		replacements: [{
			from: "'@{fa-font-path}/fontawesome-webfont.eot?v=@{fa-version}'",
			to: "'@{font-fontawesome-webfont_eot}'"
		},{
			from: "'@{fa-font-path}/fontawesome-webfont.eot?#iefix&v=@{fa-version}'",
			to: "'@{font-fontawesome-webfont_eot}'"
		},{
			from: "'@{fa-font-path}/fontawesome-webfont.woff?v=@{fa-version}'",
			to: "'@{font-fontawesome-webfont_woff}'"
		},{
			from: "'@{fa-font-path}/fontawesome-webfont.ttf?v=@{fa-version}'",
			to: "'@{font-fontawesome-webfont_ttf}'"
		},{
			from: "'@{fa-font-path}/fontawesome-webfont.svg?v=@{fa-version}#fontawesomeregular'",
			to: "'@{font-fontawesome-webfont_svg}'"
		},{
			from: "'@{fa-font-path}/FontAwesome.otf'",
			to: "'@{font-FontAwesome_otf}'"
		}]
	},
	bootstrap1: {
		src: ['bowerBuild/less/bootstrap/variables.less'],
		dest: 'bowerBuild/less/bootstrap/variables.less',
		replacements: [{
			from: "//## Define colors for form feedback states and, by default, alerts.",
			to: "//## Define colors for form feedback states and, by default, alerts.\n@state-purple-text:             #743D74;\n@state-purple-bg:               #C266C2;\n@state-purple-border:           darken(spin(@state-purple-bg, -10), 5%);\n@alert-purple-bg:            @state-purple-bg;\n@alert-purple-text:          @state-purple-text;\n@alert-purple-border:        @state-purple-border;\n@brand-purple:          #8A008A;\n@btn-purple-color:              #fff;\n@btn-purple-bg:                 @brand-purple;\n@btn-purple-border:             darken(@btn-purple-bg, 5%);\n@label-purple-bg:            @brand-purple;\n@progress-bar-purple-bg:     @brand-purple;\n"
		}]
	},
	bootstrap2: {
		src: ['bowerBuild/less/bootstrap/alerts.less'],
		dest: 'bowerBuild/less/bootstrap/alerts.less',
		replacements: [{
			from: "// Generate contextual modifier classes for colorizing the alert.",
			to: "// Generate contextual modifier classes for colorizing the alert.\n.alert-purple { .alert-variant(@alert-purple-bg; @alert-purple-border; @alert-purple-text); }\n"
		}]
	},
	bootstrap3: {
		src: ['bowerBuild/less/bootstrap/progress-bars.less'],
		dest: 'bowerBuild/less/bootstrap/progress-bars.less',
		replacements: [{
			from: "// Variations",
			to: "// Variations\n.progress-bar-purple { .progress-bar-variant(@progress-bar-purple-bg); }\n"
		}]
	},
	bootstrap4: {
		src: ['bowerBuild/less/bootstrap/labels.less'],
		dest: 'bowerBuild/less/bootstrap/labels.less',
		replacements: [{
			from: "// Contextual variations (linked labels get darker on :hover)",
			to: "// Contextual variations (linked labels get darker on :hover)\n.label-purple { .label-variant(@label-purple-bg); }\n"
		}]
	}
   }
  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-yui-compressor');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-bower');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-rename');
  grunt.loadNpmTasks('grunt-closure-compiler');
  grunt.loadNpmTasks('grunt-base64-less');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-phplint');
  grunt.loadNpmTasks('grunt-phpcs');
  grunt.loadNpmTasks('grunt-php-analyzer');
  grunt.loadNpmTasks('grunt-groc');
  grunt.loadNpmTasks('grunt-banner');

  grunt.registerTask('phpLint', ['phplint:all']);
  grunt.registerTask('php', ['phplint:all','phpcs'/*,'php_analyzer:application'*/]);

  // local
  grunt.registerTask('local', ['clean:dist','mkdir','bowerSetup','concat:bowerUncompiled','buildDebugJS','lintJS','buildLessDebug','buildHtmlDebug']);
  grunt.registerTask('localBower', ['bowerSetup','concat:bowerUncompiled']);  
  grunt.registerTask('localJS', ['buildDebugJS','lintJS']);
  grunt.registerTask('localLess', ['buildLessDebug']);
  grunt.registerTask('localHtml', ['buildHtmlDebug']); 
  grunt.registerTask('lintGrunt', ['jshint:gruntfile']);
  grunt.registerTask('lintJS', ['jshint:src']);

  grunt.registerTask('default', ['local']);

  grunt.registerTask('bowerSetup', ['bower','copy:main','clean:bowerBuildUnneeded','replace:fontAwesomeInline','replace:bootstrap1','replace:bootstrap2','replace:bootstrap3','replace:bootstrap4','rename:fixDep','rename:fixDep2'/*,'rename:fixDep3','rename:fixDep4'*/]);
  grunt.registerTask('bowerBuild', ['bowerSetup','bowerBuildCompile','bowerFlatten']);
  grunt.registerTask('bowerBuildCompile', ['closure-compiler:bower1','closure-compiler:bower2','closure-compiler:bower3']);
  grunt.registerTask('bowerFlatten', ['concat:js']);

  grunt.registerTask('buildImg', ['imagemin']);
  grunt.registerTask('buildFonts', ['base64Less:fonts']);

  grunt.registerTask("buildHtmlDebug", ['concat:debugLocal']);
  grunt.registerTask('buildHtml', ['htmlmin:dev']);

  grunt.registerTask('buildLessDebug', ['less','concat:css']);
  grunt.registerTask('buildLess', ['less','concat:css','cssmin']);

  grunt.registerTask('buildDebugJS', ['concat:app']);
  grunt.registerTask('buildJS', ['closure-compiler:app1','closure-compiler:app2','closure-compiler:app3','closure-compiler:app4','closure-compiler:app5','closure-compiler:app6','closure-compiler:app7','closure-compiler:app8','concat:appCompiled']);

  grunt.registerTask('docHtaccess', 'Create .htaccess file for ./doc', function() {
    grunt.file.write('./doc/.htaccess', 'ExpiresActive Off\nRewriteEngine On\nRewriteRule ^([^.?]+).html(.*)$ $1.php [PT]');
  });

  grunt.registerTask('devTest', ['clean:dist','mkdir','lintGrunt','lintJS','bowerSetup','concat:bowerUncompiled','less','phpLint']);
  grunt.registerTask('git', ['clean:git']);
  grunt.registerTask('docDeploy', ['clean:doc','groc','copy:doc','docHtaccess','usebanner:doc']);
  grunt.registerTask('deploy', ['clean:dist','mkdir','copy:site','bowerBuild','buildFonts','buildLess','buildHtml','buildJS','copy:deployBuild','clean:build','phpLint']);
};

