NewStart Age JS Client
========================================
An prototype client made to test [sheetengine](https://sheetengine.codeplex.com/). Uses [jabber2ws](https://bitbucket.org/subasteve/jabber2ws) for friends feature. Wasn't as performant as hoped, dropped the project.

Prerequisites
-------------
1. Node.js
2. npm
4. bower (npm install -g bower)

Compile Package Only
-------------
```shell
grunt build
```

FirstRun       
-------------
Run the FirstRun script to download packages via npm and bower.
Then run
```shell
grunt
```
This will copy dependencies over needed to build. After first run grunt build can be used utill the need to update packages.
