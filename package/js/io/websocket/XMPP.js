/*
 * # Websocket Webworkers
 * @package NewstartAge.io.websocket
 * @module XMPP
 *
 * @description 
 * Allows the offload of IO to a background thread.  
 * This socket is used for communication to   
 * `jabber.newstartrs3.com` over port `5000`  
 * Data going over this socket will be xml [XMPP protocol](http://xmpp.org/rfcs/rfc3921.html)
 * @howto
 * [webworkers docs](https://developer.mozilla.org/en-us/docs/web/api/web_workers_api/basic_usage)  
 * [websocket docs](https://developer.mozilla.org/en-us/docs/websockets)  
 */
(function (){
	/*
	 * Setup namespaces  
	 * *Only allow one instance*
	 */
	NewstartAge.io = NewstartAge.io || {};
	NewstartAge.io.websocket = NewstartAge.io.websocket || {};
	NewstartAge.io.websocket.XMPP = {};
	var initilized = false,
	destructed = false;

	/*
	 * Constructor of XMPP  
	 * *Only one instance of this class allowed*
	 * @public
	 * @constructor
	 * @method init
	 * @param {String} server DNS hostname of the server
	 * @param {int} port Port of server to connect to
	 * @param {function()} onOpen Callback when socket is open
	 * @param {function(data)} onMessage Callback when message is recieved
	 * @example 
	 * ```js
	 * var xmppSocket = NewstartAge.io.websocket.XMPP.init("jabber.newstartrs3.com",5000,function onOpen(){
	 * 	console.log("onOpen");
	 * }, function onMessage(data){
	 * 	console.log(data);
	 * }, function onClose(){
	 * 	console.log("onClose");
	 * });
	 * ```
	 * @return {XMPP}
	 */
	NewstartAge.io.websocket.XMPP.init = function(server,port,onOpen,onMessage,onClose){
		if(!initilized){
			/*
			 * Webworker process  
			 * *This is the best way to inline webworkers*
			 * @private
			 * @method workerInline
			 * @param {Object} e Contains the data the webworker uses
			 */
			var workerInline = function(e){ 
				var data = e.data,
				self = this;
				
				if(data.type === "init" && !self.initilizedSocket){
					if(typeof WebSocket === "undefined"){
						self.postMessage("error: WebSockets: Your browser doesn't support websockets in webworker please switch to a browser that does.");
						return null;
					}

					self.socket = null;
					self.initilizedSocket = false;
					self.destructedSocket = true;
					self.onCloseMessageDisabled = false;

					//Setup websockets
            				self.socket = new WebSocket('ws://' + data.server + ':'+data.port+'/', "xmpp");
            				self.socket.onopen = function (evt) {// jshint ignore:line 
						self.initilizedSocket = true;
						self.destructedSocket = false;
						self.postMessage("onopen");
					};
					self.socket.onclose = function (evt) {// jshint ignore:line
						self.destructedSocket = true;
						self.initilizedSocket = false; 
						if(!self.onCloseMessageDisabled){
							self.postMessage("error: WebSockets: Unexpectedly closed... WebSocket server could be offline.");
						}
						self.postMessage("onclose");
					};
            				self.socket.onerror = function (evt) {
						self.postMessage("error: WebSockets: "+evt.data);
					};
            				self.socket.onmessage = function (evt) {
            					self.postMessage(evt.data);
					};
				}else if(data.type === "closeMsgDisable" && self.initilizedSocket){
					self.onCloseMessageDisabled = true;
				}else if(data.type === "close" && self.initilizedSocket){
					self.socket.close();	
				}else if(data.type === "send" && self.initilizedSocket && data.msg){
					//console.log("send",data.msg);
					self.socket.send(data.msg);		
				}else{
					console.log("UNKNOWN",self.initilizedSocket,data.type);
				}
			},
			/*
			 * Create blob to store the webworker  
			 * *Only way to inline webworkers*
			 * @private
			 * @property blob
			 */
			blob = new Blob(["onmessage ="+workerInline.toString()], { type: "text/javascript" }),
			/*
			 * Actual webworker object
			 * @private
			 * @property worker
			 */
			worker = new Worker(window.URL.createObjectURL(blob));
			
			/*
			 * Webworker onmessage callback  
			 * Provides feedback via onOpen and onMessage callbacks
			 * @private
			 * @subscribes worker.onmessage
			 * @method onmessage
			 * @param {Object} e Data from webworker
			 */
			worker.onmessage = function onmessage(e) { 
				//if(typeof e.data === "string"){
					
					if(e.data === "onopen"){
						onOpen();
						initilized = true;
						destructed = true;
					}else if(e.data === "onclose"){
						onClose();
						initilized = false;
						destructed = true;
					}else if(e.data.indexOf("error: ") > -1){
						console.error(e.data.substring(7));
					}
				
				//}
				else{
					onMessage(e.data);	
				}
			};
			
			/*
			 * Startup webworker
			 */
			worker.postMessage({type: "init", server: server, port: port}); 
			
			/*
			 * Check if the connection is open
			 * @public
			 * @method isOpen
			 * @return {boolean}
			 */ 
			this.isOpen = function isOpen(){
				return initilized;
			};
			
			/*
			 * Check if the connection is closed
			 * @public
			 * @method isClosed
			 * @return {boolean}
			 */
			this.isClosed = function isOpen(){
				return destructed;
			};
			
			/*
			 * Close the connection  
			 * *No way to reopen connection*
			 * @public
			 * @method close
			 */
			this.close = function close(){
				worker.postMessage({type: "close"});
			};
			
			
			/*
			 * Send data to connection  
			 * *xml should be used, no serialization or de-serialization will take place*
			 * @public
			 * @method send
			 * @example 
	 		 * ```js
	 		 * webSocket.send("<?xml version=\"1.0\"?>");
			 * ```
			 * @return {XMPP}
			 */
			this.send = function send(msg){
				worker.postMessage({type: "send", msg: msg});
				return this;
			};

			
			/*
			 * Hook to close socket before unload
			 * @private
			 * @method onbeforeunload
			 */
			window.onbeforeunload = function onbeforeunload() {
            			worker.postMessage({type: "closeMsgDisable"});	
            			this.close();
        		};
			return this;
		}else{
			return null;
		}
	};
})();

