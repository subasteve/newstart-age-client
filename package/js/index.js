/*
 * # Newstart Age of Empires Client
 * @module Index
 *
 * @description 
 * This will be a remake of Age of Empires with vast improvements   
 */
(function () {
    var loaded = false;

    NewstartAge.init = function () {
        if (!loaded) {
        	loaded = true;	
        	NewstartAge.Router.init();
        }
    };

}());
NewstartAge.init();
