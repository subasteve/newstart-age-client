(function () {
	
	NewstartAge.Views = NewstartAge.Views || {};

	var self = {},
	densityMap = null,
	lastLoopTime = 0,
	sprites = new Image(),
	spritesLoaded = false,
	centertile = {x:0,y:0},
	map = [],
	activeMap = [],
	size = 20,
	updateMap = null,	
	tileSize = 400,
	calcRedraw = false,
	redraw = false;

	self.controller = function controller(){
		self.viewModel = viewModel;
		return this;
	};

	self.viewModel = function viewModel(){
		var self = {};
		self.first = m.prop(true);
		self.aspect = m.prop("-1");
		self.width = m.prop(800);
		self.height = m.prop(600);
		self.lastIndex = 0;
		self.trees = [];
		self.players = [];
		self.playerNames = [];
		self.gamepads = [];
		self.gamepadSupportChecked = m.prop(false);
		self.gamepadSupported = m.prop(false);
		return self;
	};

	var viewModel = new self.viewModel();

	self.view = function view(controller){ // jshint ignore:line
		return [
			m("canvas", {id: "maincanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			(controller.viewModel.players.length > 1) ? controller.viewModel.players.filter(function dropFirst(player){
				if(player.id === 0){
					return false;
				}
				return true;
			}).map(function listPlayers(player){
				return m("canvas", {id: "maincanvas"+player.id, width: controller.viewModel.width(), height: controller.viewModel.height()});
			}) : "",
				
			/*
			m("canvas", {id: "baseshadowcanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			m("canvas", {id: "backgroundcanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			m("canvas", {id: "tempcanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			m("canvas", {id: "tempshadowcanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			m("canvas", {id: "shadowcanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			m("canvas", {id: "compositecanvas", width: controller.viewModel.width(), height: controller.viewModel.height()}),
			*/
			m("div", {class: "debugPanel"}, [
				m("h3", (spritesLoaded) ? "Sprites Loaded" : "Sprites Not Loaded"),
				m("h3", "Gamepads "+controller.viewModel.gamepads.length),
				controller.viewModel.gamepads.map(function listController(controller){
					var pressedButtons = controller.buttons.filter(function filterButtons(button){
						if(button.pressed){
							return true;
						}
						return false;
					});
					var array = [], id = 0;
					controller.axes.map(function createAxesObject(value){
						array.push({
							id: id++,
							value: value
						});
					});
					var movedAxes = array.filter(function filterAxes(axis){
						if(axis.value !== 0){
							return true;
						}
						return false;
					});
					return [
						m("p", "Controller "+controller.index+" "+controller.id),
						(pressedButtons.length > 0) ?
						m("p", "buttons " + pressedButtons.map(function getButtons(button){
							return button.id;
						}) +" pressed") : "",
						(movedAxes.length > 0) ?
						m("p", "Axes " + movedAxes.map(function getAxes(axis){
							return axis.id+"["+axis.value+"]";

						}) +" moved") : ""
					];
				})	
			])

		];
	};

	self.getStyle = function getStyle(className){
		for(var i = 0; i < document.styleSheets.length; i++){
			var classes = document.styleSheets[i].rules || document.styleSheets[i].cssRules;
			for (var x = 0; x < classes.length; x++) {
				if (classes[x].selectorText === className) {
					return (classes[x].cssText) ? classes[x].cssText : classes[x].style.cssText;
				}
			}
		}
	};

	self.styleGetProperty = function styleGetProperty(style,property){
		property = property+":";
		var returnStyle = "";
		returnStyle = style.substring(style.indexOf(property)+property.length);
		returnStyle = returnStyle.substring(0,returnStyle.indexOf(";"));
		return returnStyle.trim();
	};

	self.styleGetItem = function styleGetItem(id){
		var itemStyle = self.getStyle(".item-"+id+"-Image"),
		itemWidth = self.styleGetProperty(itemStyle,"width"),
		itemHeight = self.styleGetProperty(itemStyle,"height"),
		position = self.styleGetProperty(itemStyle,"background-position"),
		item = {};
		
		position = position.split(" ");

		item.x = parseInt(position[0].substring(0,position[0].length-2));
		item.y = parseInt(position[1].substring(0,position[1].length-2));
		
		itemWidth = itemWidth.substring(0,itemWidth.length-2);
		itemHeight = itemHeight.substring(0,itemHeight.length-2);
		
		item.width = parseInt(itemWidth);
		item.height = parseInt(itemHeight);

		return item;
	};

	function updateSheets(){
        	var tileHalf = tileSize+100, 
	        boundary = { xmin: centertile.x * tileSize - tileHalf, xmax: centertile.x * tileSize + tileHalf, ymin: centertile.y * tileSize - tileHalf, ymax: centertile.y * tileSize + tileHalf };
        


		// add new sheets
        	for (var x=0;x<map.length;x++) {
          		var sheetinfo2 = map[x];
          		if (sheetinfo2.centerp.x < boundary.xmin || sheetinfo2.centerp.x > boundary.xmax || sheetinfo2.centerp.y < boundary.ymin || sheetinfo2.centerp.y > boundary.ymax){
            			continue;
			}

          		if (!sheetinfo2.added && !sheetinfo2.removed) {
            			sheetinfo2.sheet = sheetinfo2.init();
            			sheetinfo2.added = true;
          			activeMap.push(sheetinfo2);
          		}
        	}

		// translate background
        	sheetengine.scene.translateBackground(
          		{x:centertile.x*tileSize,y:centertile.y*tileSize}, 
          		{x:centertile.x*tileSize,y:centertile.y*tileSize}
        	);
	}

	function loadAndRemoveSheets(oldcentertile, centertile) {
	        var tileHalf = tileSize+100, 
	        boundary = { xmin: centertile.x * tileSize - tileHalf, xmax: centertile.x * tileSize + tileHalf, ymin: centertile.y * tileSize - tileHalf, ymax: centertile.y * tileSize + tileHalf };
        
	        // remove sheets that are far
	        for (var i=0;i<map.length;i++) {
	          	var sheetinfo = map[i];
          		if (sheetinfo.centerp.x < boundary.xmin || sheetinfo.centerp.x > boundary.xmax || sheetinfo.centerp.y < boundary.ymin || sheetinfo.centerp.y > boundary.ymax) {
            			if (sheetinfo.added) {
            				if(_.isArray(sheetinfo.sheet)){
						for(var y = 0; y < sheetinfo.sheet.length; y++){
							sheetinfo.sheet[y].destroy();
						}
						sheetinfo.added = false;
					}else{
              					sheetinfo.sheet.destroy();
              					sheetinfo.added = false;
					}
            			}
          		}
        	}

        	activeMap = [];
        
        	// add new sheets
        	for (var x=0;x<map.length;x++) {
          		var sheetinfo2 = map[x];
          		if (sheetinfo2.centerp.x < boundary.xmin || sheetinfo2.centerp.x > boundary.xmax || sheetinfo2.centerp.y < boundary.ymin || sheetinfo2.centerp.y > boundary.ymax){
            			continue;
			}
            
          		if (!sheetinfo2.added && !sheetinfo2.removed) {
            			sheetinfo2.sheet = sheetinfo2.init();
            			sheetinfo2.added = true;
          			activeMap.push(sheetinfo2);
          		}
        	}
        
        	// translate background
        	sheetengine.scene.translateBackground(
          		{x:oldcentertile.x*tileSize,y:oldcentertile.y*tileSize}, 
          		{x:centertile.x*tileSize,y:centertile.y*tileSize}
        	);

		/*
		if(!_.isUndefined(player)){
        		sheetengine.scene.setCenter({x:player.centerp.x, y:player.centerp.y, z:0});
		}else{
			sheetengine.scene.setCenter({x:centertile.x*200, y:centertile.y*200, z:0});
		}
		*/
      	}

	function createGroundItem(obj){
		var item = self.styleGetItem(obj.id);
		map.push({
			centerp: {x: obj.x, y: obj.y, z: 0},
            		orientation: {alphaD:0,betaD:0,gammaD:0},
            		size: {w:item.width,h:item.height},
            		init: function() {
             			var groundSheet = new sheetengine.Sheet(this.centerp, this.orientation, this.size);
              			groundSheet.context.drawImage(sprites, Math.abs(item.x),Math.abs(item.y), item.width, item.height, 0,0, item.width, item.height);
              			return groundSheet;

            		}
		});
		//var groundItemSheet = new sheetengine.Sheet({x:obj.x,y:obj.y,z:0}, {alphaD:0,betaD:0,gammaD:0}, {w:item.width,h:item.height});

		//groundItemSheet.context.drawImage(sprites, Math.abs(item.x),Math.abs(item.y), item.width, item.height, 0,0, item.width, item.height);
		//sheetengine.calc.calculateChangedSheets();
		//sheetengine.drawing.drawScene();
		//return groundItemSheet;						
	}

	function removeTree(tree){
		map.map(function findTree(obj){
			if(obj.id){
				if(obj.id === tree){
					obj.removed = true;
					obj.object.map(function getTree(obj){
						densityMap.removeSheet(obj);
						obj.destroy();
					});
				}
			}
		});
	}

	// define some sheets: create a pine tree
      	function createTree(id, removed, x, y, z, w, h){ // jshint ignore:line
      		//var sheet4 = new sheetengine.Sheet({x: x, y: y, z: z}, {alphaD:0,betaD:0,gammaD:0}, {w: w, h: h});
      		//var sheet5 = new sheetengine.Sheet({x: x, y: y, z: z}, {alphaD:0,betaD:0,gammaD:90}, {w: w, h: h});
      
      		function drawPineTexture(context) {
       			context.fillStyle='#BDFF70';
       			context.beginPath();
       			context.moveTo(40,0);
       			context.lineTo(60,30);
       			context.lineTo(50,30);
       			context.lineTo(70,60);
       			context.lineTo(10,60);
       			context.lineTo(30,30);
       			context.lineTo(20,30);
       			context.fill();
       			context.fillStyle='#725538';
       			context.fillRect(35,60,10,20);
      		}
      		
      		//drawPineTexture(sheet4.context);
      		//drawPineTexture(sheet5.context);
      
		map.push({
			centerp: {x: x, y: y, z: z},
            		orientation: {alphaD:0,betaD:0,gammaD:0},
            		size: {w:w,h:h},
            		tree: true,
            		id: id,
            		removed: removed,
            		object : [],
            		init: function() {
             			var sheet = new sheetengine.Sheet(this.centerp, this.orientation, this.size),
              			sheet2 = new sheetengine.Sheet(this.centerp, {alphaD:0,betaD:0,gammaD:90}, this.size);
              			drawPineTexture(sheet.context);
              			drawPineTexture(sheet2.context);
              			this.object = [sheet,sheet2];
              			return [sheet,sheet2];
            		}
		});

      		return [];
	}
	
	function drawTrees(){
		var t0,t1;
		
		t0 = performance.now();
		viewModel.trees.map(function makeTree(tree){
			var x1 = tree.x,
			y1 = tree.y,
			z = 40,
			w = 80,
			h = w;
			tree.object = createTree(tree.id,tree.removed,x1,y1,z,w,h);
		});
		t1 = performance.now();
		console.log("CreateTrees took " + ((t1 - t0)/1000) + " seconds.");
	}

	self.render = function render(canvas){
		var canvasElement = document.getElementById(canvas);
		sheetengine.scene.init(canvasElement, {w:900*2,h:500*2});
      
		//sheetengine.transforms.transformPointz = function(p) {
		//	var u = p.x;
		//	var v = p.y;
		//	var z = p.z;
		//	return { u: u, v: v, z: z };
		//};
		//sheetengine.transforms.inverseTransformPoint = function(p) {
		//	var su = Math.SQRT1_2;
    		//	var sv = su / 2;
    
    		//	var x = ((p.u - sheetengine.canvasCenter.u)/su + (p.v - sheetengine.canvasCenter.v)/sv) / 2;
    		//	var y = (p.v - sheetengine.canvasCenter.v)/sv - x;
    		//	return { x:Math.floor(x), y:Math.floor(y), z:null };
		//};
		//sheetengine.viewSource = { x: 0, y: 0, z: 0};
		
		// adjust the light source
		//sheetengine.shadows.lightSource = { x: 1, y: -1, z: -1 };
		//sheetengine.shadows.lightSourcep1 = { x: 1, y: 2, z: -1 };  // perpendicular to lightsource, scalar prod is 0 : 1x -1y -1z = 0
		//sheetengine.shadows.lightSourcep2 = { x: 3, y: 0, z: 3 };  // perpendicular both to lightsource and p1 (ls x p1 = p2)
		//sheetengine.shadows.shadowAlpha = 0.8;
		//sheetengine.shadows.shadeAlpha = 0.6;

      		// define some basesheets
      		for (var x=-size; x<=size; x++) {
        		for (var y=-size; y<=size; y++) {
          			map.push({
          				centerp: { x: x * tileSize, y: y * tileSize, z: 0 },
          			        orientation: {alphaD: 90, betaD: 0, gammaD: 0},
          			        size: {w:tileSize,h:tileSize},
          			        init: function() { 
          			       		var basesheet = new sheetengine.BaseSheet(this.centerp, this.orientation, this.size);
          				        basesheet.color = '#5D7E36';
          				        return basesheet;
          				}
          			});// jshint ignore:line
        		}
      		}

		//var t0,t1;
		
		drawTrees();
		
		// generate a density map from the sheets
		densityMap = new sheetengine.DensityMap(5);
		densityMap.addSheets(sheetengine.sheets);

		if(!spritesLoaded){
			sprites.onload = function() {
				m.startComputation();
				spritesLoaded = true;
				m.endComputation();
				
				viewModel.players.map( function updateForSprites(player){
					var id = player.id;
					delete viewModel.players[id];
					setupPlayer(id);	
				});
				
				calcRedraw = true;
				redraw = true;
			};
			sprites.src = "http://static.newstartrs3.com/sprites.png";
		}
		
		//self.zoom(2);

		loadAndRemoveSheets({x:0,y:0}, centertile);

		calcRedraw = true;
		redraw = true;

      		// draw the scene
      		//t0 = performance.now();
      		//sheetengine.calc.calculateAllSheets();
      		//t1 = performance.now();
      		//console.log("calculateAllSheets took " + ((t1 - t0)/1000) + " seconds.");
      		//t0 = performance.now();
      		//sheetengine.drawing.drawScene(true);
      		//t1 = performance.now();
      		//console.log("drawScene took " + ((t1 - t0)/1000) + " seconds.");
	};

	self.zoom = function zoom(amount){
		sheetengine.context.scale(amount,amount);
		sheetengine.context.translate(
			-sheetengine.canvas.width/(2*amount)*(amount-1),
			-sheetengine.canvas.height/(2*amount)*(amount-1));
	};

	function SetupEquipment(){
		var Equipment = {
			body: self.styleGetItem(581),
			item1: self.styleGetItem(1351),
			item2: self.styleGetItem(1173),
			legs: self.styleGetItem(1077),
			cape: self.styleGetItem(1007),
			helment: self.styleGetItem(1151)
		};
		Equipment.bodySheet = new sheetengine.Sheet({x:0,y:0,z:25}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.body.width,h:Equipment.body.height});
		Equipment.bodyBackSheet = new sheetengine.Sheet({x:0,y:-1,z:10}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.cape.width,h:Equipment.cape.height+10});
		Equipment.headSheet = new sheetengine.Sheet({x:0,y:0,z:45}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.helment.width,h:Equipment.helment.height});
		Equipment.legSheet = new sheetengine.Sheet({x:0,y:0,z:10}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.legs.width,h:Equipment.legs.height});
		Equipment.hand1Sheet = new sheetengine.Sheet({x:0,y:2,z:23}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.item1.width,h:Equipment.item1.height});
		Equipment.hand2Sheet = new sheetengine.Sheet({x:-2,y:0.5,z:15}, {alphaD:0,betaD:0,gammaD:0}, {w:Equipment.item2.width+10,h:Equipment.item2.height+10});
		return Equipment;
	}

	function applyEquipmentTransforms(equipment){
		if(spritesLoaded){
        		equipment.bodyBackSheet.context.translate(-7,-5);
        		equipment.bodyBackSheet.context.rotate(-0.5); 
        		equipment.bodyBackSheet.context.drawImage(sprites, Math.abs(equipment.cape.x),Math.abs(equipment.cape.y), equipment.cape.width, equipment.cape.height, 0,0, equipment.cape.width, equipment.cape.height+10);
        		equipment.hand2Sheet.context.translate(7,5);
        		equipment.hand2Sheet.context.rotate(-0.5);
        		equipment.hand2Sheet.context.drawImage(sprites, Math.abs(equipment.item2.x),Math.abs(equipment.item2.y), equipment.item2.width, equipment.item2.height, 0,0, equipment.item2.width, equipment.item2.height);
        		equipment.hand1Sheet.context.drawImage(sprites, Math.abs(equipment.item1.x),Math.abs(equipment.item1.y), equipment.item1.width, equipment.item1.height, 0,0, equipment.item1.width, equipment.item1.height);
        		equipment.headSheet.context.drawImage(sprites, Math.abs(equipment.helment.x),Math.abs(equipment.helment.y), equipment.helment.width, equipment.helment.height, 0,0, equipment.helment.width, equipment.helment.height);
        		equipment.legSheet.context.drawImage(sprites, Math.abs(equipment.legs.x),Math.abs(equipment.legs.y), equipment.legs.width, equipment.legs.height, 0,0, equipment.legs.width, equipment.legs.height);
			equipment.bodySheet.context.globalCompositeOperation="destination-over";
			equipment.bodySheet.context.drawImage(sprites, Math.abs(equipment.body.x),Math.abs(equipment.body.y), equipment.body.width, equipment.body.height, 0,0, equipment.body.width, equipment.body.height);
		
		}
	}

	function createChar(centerp,equipment){
		return new sheetengine.SheetObject(
       				centerp, 
       				{alphaD:0,betaD:0,gammaD:90}, 
       				[
       					equipment.legSheet,
       					equipment.bodySheet,
       					equipment.headSheet,
       					equipment.hand2Sheet,
       					equipment.hand1Sheet,
       					equipment.bodyBackSheet
       				], 
       				{
       					w:equipment.legs.width+equipment.body.width, 
       					h:equipment.legs.height+equipment.body.height+equipment.helment.height, 
       					relu:equipment.legs.width, 
       					relv:equipment.legs.height+30
       				});

	}


	function setupPlayer(i){
		if(_.isUndefined(viewModel.players[i])){
			viewModel.players[i] = self.Char({x:0,y:0,z:0});
			viewModel.playerNames[i] =  "Player"+i;
			webSocket.send({players: viewModel.playerNames});
			viewModel.players[i].keys = {};
			viewModel.players[i].id  = i;
			viewModel.players[i].direction = "up";
			if(viewModel.players[i].centerp){
				sheetengine.scene.setCenter({x:viewModel.players[i].centerp.x, y:viewModel.players[i].centerp.y, z:0});
			}
		}	
	}

	self.Char = function Char(centerp){
		if(!spritesLoaded){
			return {
				animationState: 0
			};
		}
		var equipment = new SetupEquipment();

		applyEquipmentTransforms(equipment);
		
       		// define character object
       		var character = createChar(centerp,equipment);
          
		character.equipment = equipment;
        	character.animationState = 0;
        	return character;
	};

	self.setCenter = function setCenter(x,y,z){
		sheetengine.scene.setCenter({x: x, y: y, z: z});
		sheetengine.drawing.drawScene();
	};

	self.moveCenter = function moveCenter(x,y,z){
		sheetengine.scene.moveCenter({x: x, y: y, z: z});
		sheetengine.drawing.drawScene();
	};

	function swingAxe(character){// jshint ignore:line
		var state = Math.floor( (character.animationState % 8) / 4);
		var dir = (state === 0 || state === 3) ? -1 : 1;
		var value = dir * Math.PI/64;

		character.rotateSheet(character.hand1, {x:1,y:0,z:0}, {x:1,y:0,z:0}, value);
	}

	// function for animating character's sheets
    	function animateCharacter(character) { // jshint ignore:line
        	var state = Math.floor( (character.animationState % 8) / 4);
        	var dir = (state === 0 || state === 3) ? -1 : 1;
        
        	var value = dir * Math.PI/32;
        	//console.log(value, character.hand1.p2.y);

        	character.rotateSheet(character.hand1, {x:1,y:0,z:0}, {x:1,y:0,z:0}, value);
        	//character.rotateSheet(character.leg1, {x:0,y:0,z:0}, {x:0,y:0,z:0}, dir * Math.PI/8);
        	//character.rotateSheet(character.leg2, {x:0,y:0,z:0}, {x:0,y:0,z:0}, -dir * Math.PI/8);
        	
      	}

	function distance(x, y, x0, y0){// jshint ignore:line
		    return Math.sqrt((x -= x0) * x + (y -= y0) * y);
	}

	self.gameLoop = function gameLoop() {
		
		if(spritesLoaded && densityMap !== null){
			viewModel.players.map( function updatePlayer(player){
				if(!_.isNull(updateMap)){
					loadAndRemoveSheets(updateMap.oldSheet, updateMap.newSheet);	
					calcRedraw = true;
					redraw = true;
					updateMap = null;
				}
				
				var dx = player.centerp.x,
        			dy = player.centerp.y;
        			var keys = player.keys,
        			currentKeys = JSON.stringify({player: {keys: keys}});
        			if(currentKeys !== player.lastKeys){
        				webSocket.send({Player: {keys: keys}});
        			}
				player.lastKeys = currentKeys;
				if(_.isUndefined(player.moveDirection)){
					return;
				}

				if(!_.isUndefined(player.moveDirection.y)){
					dy = player.moveDirection.y;
				}
				if(!_.isUndefined(player.moveDirection.x)){
					dx = player.moveDirection.x;
				}
				player.setOrientation({alphaD:0,betaD:0,gammaD:player.moveDirection.angle});
				
				if(_.isUndefined(player.jumpspeed) || _.isUndefined(player.jump)){
					player.jump = 0;
					player.jumpspeed = 0;
				}

				if(player.keys.a){
					player.keys.a = false;
					player.animationState++;
					swingAxe(player);
					var treeRemoved = false;
					map.map(function chopTree(tree){
						if(tree.tree){
							if(!tree.removed && distance(tree.centerp.x,tree.centerp.y,player.centerp.x,player.centerp.y) <= 40){
								if(!treeRemoved){
									webSocket.send({chopTree: tree.id});
									treeRemoved = true;
								}
							}
						}
					});
					calcRedraw = true;
				
				}
        
        			// character constantly falls
        			//player.jumpspeed -= 2;
        
        			// get allowed target point. character's height is 20, and character can climb up to 10 pixels
        			//var targetInfo = densityMap.getTargetPoint(player.centerp, {x:dx, y:dy, z:player.jumpspeed}, 20, 10);
        			//var allowMove = targetInfo.allowMove;
        			//var targetp = targetInfo.targetp;
        			//var stopFall = targetInfo.stopFall;
        
        			// if character stops falling, reset jump info
        			//if (stopFall) {
         	 		//	player.jumpspeed = 0;
         	 		//	player.jump = 0;
        			//}
        			//
        			var targetp = {
					x: dx,
					y: dy,
					z: 0
				};
        
        			var moved = targetp.x !== player.centerp.x || targetp.y !== player.centerp.y || targetp.z !== player.centerp.z;
        			if (moved) {
          				// move character to target point
          				player.setPosition(targetp);

          				//animateCharacter(player);
          				//player.animationState++;
          
          				// move center
          				sheetengine.scene.setCenter({x:player.centerp.x, y:player.centerp.y, z:0});
          
					calcRedraw = true;

        			}
				delete player.moveDirection;
			});
		}
		if(calcRedraw && !redraw){
			calcRedraw = false;
			sheetengine.calc.calculateChangedSheets();
			sheetengine.drawing.drawScene();
		}else if(calcRedraw && redraw){
			calcRedraw = false;
			redraw = false;
			sheetengine.calc.calculateAllSheets();
			sheetengine.drawing.drawScene(true);
		}
		lastLoopTime = performance.now();
	};

	self.updateGamepads = function updateGamepads(gamepads) {
		m.startComputation();
		viewModel.gamepads = gamepads;
		self.updateGamepads2();
		m.endComputation();
	};

	self.updateButtons = function updateButtons(buttons, axes, gamepadId) {// jshint ignore:line
		m.startComputation();
		self.processButtons(buttons, axes, gamepadId);
		for(var i = 0; i < buttons.length; i++){
			buttons[i].id = i;
		}
		self.updateGamepads2();
		if(!_.isUndefined(viewModel.gamepads[gamepadId])){
			viewModel.gamepads[gamepadId].axes = axes;
			viewModel.gamepads[gamepadId].buttons = buttons;
		}
		m.endComputation();
	};

	self.updateGamepads2 = function updateGamepads2() {
		for(var i = 0; i < viewModel.gamepads.length; i++){
			setupPlayer(i);
		}
	};

	self.processButtons = function processButtons(buttons, axes, gamepadId){// jshint ignore:line
		if(!_.isUndefined(viewModel.players[gamepadId])){
			viewModel.players[gamepadId].keys.u = false;
			viewModel.players[gamepadId].keys.d = false;
			viewModel.players[gamepadId].keys.l = false;
			viewModel.players[gamepadId].keys.r = false;
			viewModel.players[gamepadId].keys.a = false;
			viewModel.players[gamepadId].keys.b = false;
			viewModel.players[gamepadId].keys.select = false;
			viewModel.players[gamepadId].keys.start = false;
			if(axes[1] === -1){
				viewModel.players[gamepadId].keys.u = true;
			}
			if(axes[1] === 1){
				viewModel.players[gamepadId].keys.d = true;
			}
			if(axes[0] === -1){
				viewModel.players[gamepadId].keys.l = true;
			}
			if(axes[0] === 1){
				viewModel.players[gamepadId].keys.r = true;
			}
			if(buttons[1].pressed){
				viewModel.players[gamepadId].keys.a = true;
			}
			if(buttons[2].pressed){
				viewModel.players[gamepadId].keys.b = true;
			}
			if(buttons[8].pressed){
				viewModel.players[gamepadId].keys.select = true;
			}
			if(buttons[9].pressed){
				viewModel.players[gamepadId].keys.start = true;
			}
		}
	};

	self.process = function process(){
		if(viewModel.first()){
			m.startComputation();
			viewModel.first(false);

			/*
			for(var i = 0; i < 50; i++){
				viewModel.trees.push({removed: false, x: Math.floor(Math.random() * window.innerWidth) + -(window.innerWidth/2), y: Math.floor(Math.random() * window.innerHeight) + -(window.innerHeight/2)});
			}*/


			viewModel.width(window.innerWidth);
			viewModel.height(window.innerHeight);
			viewModel.aspect(window.innerWidth / window.innerHeight);
			m.endComputation();
			
			setTimeout(function wait(){
				self.render('maincanvas');
				sheetengine.canvas.width = window.innerWidth;
				sheetengine.canvas.height = window.innerHeight;
				//self.zoom(2);
				sheetengine.drawing.drawScene();
			}, 200);

			window.onresize = function onresize(){
				m.startComputation();
				viewModel.width(window.innerWidth);
				viewModel.height(window.innerHeight);
				viewModel.aspect(window.innerWidth / window.innerHeight);
				sheetengine.canvas.width = window.innerWidth;
				sheetengine.canvas.height = window.innerHeight;
				m.endComputation();
				//self.zoom(2);
				
				//self.render();
				setTimeout(function wait(){
				//self.zoom(2);	
				sheetengine.drawing.drawScene();
				}, 200);
			};
		}
		
	};


	NewstartAge.Views.Main = self;
	gamepadSupport.init();
	var webSocket = NewstartAge.io.websocket.WebSocket.init("localhost",8080,function onOpen(){
		console.log("Websocket opened");
		webSocket.send({players: viewModel.playerNames});
	},function onMessage(data){
		//console.log(data);
		if(data.welcome){
			if(data.trees){
				viewModel.trees = data.trees;	
				drawTrees();
			}
			if(data.groundItems){
				data.groundItems.map(function createGroundItems(item){
					createGroundItem(item);
				});	
			}

			if(viewModel.players.length > 0){
				loadAndRemoveSheets({x:viewModel.players[0].centerp.x,y:viewModel.players[0].centerp.y}, centertile);
			}else{
				loadAndRemoveSheets({x:0,y:0}, centertile);
			}

			sheetengine.calc.calculateAllSheets();
			//sheetengine.calc.calculateChangedSheets(); 
			sheetengine.drawing.drawScene(true);
		}
		if(data.removeTree && data.createGroundItem){
			removeTree(data.removeTree);
			createGroundItem(data.createGroundItem);
			
			updateSheets();

			sheetengine.calc.calculateChangedSheets();
			sheetengine.drawing.drawScene(true);
		}else if (data.removeTree){
			removeTree(data.removeTree);

			sheetengine.calc.calculateChangedSheets();
			sheetengine.drawing.drawScene(true);
		}else if(data.createGroundItem){
			createGroundItem(data.createGroundItem);
		
			updateSheets();

			//sheetengine.calc.calculateAllSheets();
			sheetengine.calc.calculateChangedSheets();
			sheetengine.drawing.drawScene();
		}

		if(data.Player){
			if(data.Equipment){
			
			}
			if(data.Position){
				
			}
			if(data.name){
			
			}
		}
		if(data.MovePlayer){
			viewModel.players[0].moveDirection = {angle: data.MovePlayer.angle, x: data.MovePlayer.x, y: data.MovePlayer.y};
		}
		if(data.UpdateMap){
			updateMap = data.UpdateMap;
		}
	});
}());
