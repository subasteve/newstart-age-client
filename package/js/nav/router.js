(function () {
	var self = {};
	self.lastRoute = "";

	self.init = function(){
		m.route.mode = "hash";

		m.route(document.body, "/Home", {
			"/": function(){},
			"/Home": {
				controller: function(){
					return NewstartAge.Views.Main.controller();
				},
				view: function(controller){
					return [NewstartAge.Views.Main.view(controller)];
				},
				process: function(){
					if(self.lastRoute !== "/Home"){
						self.lastRoute = "/Home";
						NewstartAge.Views.Main.process();
					}
				}
			}
		});
	};
	NewstartAge.Router = self;
})();
