/*
 * # Jabber Handshake
 * @package Jabber.jabber
 * @module Handshake
 *
 * @description 
 * This handshake is used for communication to   
 * `jabber.newstartrs3.com` over port `5000`  
 * @howto
 * [XMPP protocol](http://xmpp.org/rfcs/rfc3921.html)
 */
(function(){
	/*
	 * Setup namespaces  
	 * *Only allow one instance*
	 */
	var Jabber = Jabber || {};
	Jabber.jabber = Jabber.jabber || {};
	Jabber.jabber.Handshake = {};
	var initilized = false;

	/*
	 * Constructor of Handshake   
	 * *Only one instance of this class allowed*
	 * @public
	 * @constructor
	 * @method init
	 * @param {NewstartAge.io.websocket.XMPP} XMPP Websocket to jabber2ws server
	 * @example 
	 * ```js
	 * var client = Jabber.jabber.Handshake.init(XMPP);
	 * ```
	 * @return {Handshake}
	 */
	Jabber.jabber.Handshake.init = function(XMPP){
		if(!initilized){
			initilized = true;
			var self = this,
			/*
			 * Server handshake ID
			 * @private
			 * @property nonce
			 * @return {String}
			 */
			nonce = "",
			/*
			 * Client handshake ID
			 * @private
			 * @property cnonce
			 * @return {String}
			 */
			cnonce = "";

			/*
			 * Make ID for handshake process
			 * @public
			 * @method makeid
			 * @return {String}
			 */ 
			self.makeid = function makeid(){
				var text = "", 
			    	    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		        	for(var i=0; i < 25; i++){
					text += possible.charAt(Math.floor(Math.random() * possible.length));
				}
			
				return text;
			};
			
			/*
			 * Set client handshake ID
			 */ 
			cnonce = self.makeid();
			
			/*
			 * First step to init TLS
			 * @public
			 * @property jabberInit
			 * @return {boolean}
			 */
			self.jabberInit = false;
			/*
			 * TLS handshake
			 * @public
			 * @property tlsDone
			 * @return {boolean}
			 */ 
			self.tlsDone = false;
			/*
			 * Security of server
			 * @public
			 * @property SASL
			 * @return {boolean}
			 */ 
			self.SASL = false;
			/*
			 * Realm logging on to
			 * @public
			 * @property
			 * @return {String}
			 */ 
			self.realm = "";
			/*
			 * Handshake process complete
			 * @public
			 * @property complete
			 * @return {boolean}
			 */ 
			self.complete = false;
			/*
			 * Member username
			 * @public
			 * @property username
			 * @return {String}
			 */ 
			self.username = "";
			/*
			 * Algorithm used for login handshake
			 * @public
			 * @proptery algorithm
			 * @return {String}
			 */ 
			self.algorithm = "md5-sess";
			
			/*
			 * Retrieve cached responseHash
			 * @public
			 * @method cachedResponse
			 * @return {String}
			 */ 
			self.cachedResponse = function cachedResponse(){
				return jStorage.jStorage.get("tlsCachedResponse", null);
			};

			/*
			 * Create encrypted password to send to server from   
			 * Original password
			 * @public
			 * @method responseHash
			 * @param {String} password password input from form
			 * @return {String}
			 */ 
			self.responseHash = function responseHash(password){
				var userPrep = "",
				userHash = "", 
				noncePrep = "",
				nonceHash = "",
				authPrep = "",
				authHash = "",	
				totalPrep = "",
				totalHash = "";	
				//cachedHash = self.cachedResponse();
				

				//if(cachedHash !== null){
				//	return cachedHash;
				//}
				//
				
				//http://www.deusty.com/2007/09/example-please.html
				//self.username="test";
				//password="secret";
				//self.realm="osXstream.local";
				//nonce="392616736";
				//cnonce="05E0A6E7-0B7B-4430-9549-0FE1C244ABAB";

				password = Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(password));
				password = self.username+"@"+self.realm+":"+password;
				password = Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(password));

				userPrep = self.username+":"+self.realm+":"+password;
				userHash = Jabber.encryption.MD5.rstrMd5(Jabber.encryption.MD5.str2rstrUtf8(userPrep));
				noncePrep = userHash+":"+nonce+":"+cnonce;
				nonceHash = Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(noncePrep));
				authPrep = "AUTHENTICATE:xmpp/"+self.realm;
				authHash = Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(authPrep));
				totalPrep = nonceHash+":"+nonce+":"+"00000001"+":"+cnonce+":auth:"+authHash;
				totalHash = Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(totalPrep));
				//jStorage.jStorage.set("tlsCachedResponse", totalHash);
				return totalHash;
			};
			
			/*
			 * Create handshake and send to server
			 * @public
			 * @method sendChallenge
			 * @param {String} password Encrypted password
			 */
			self.sendChallenge = function sendChallenge(password){
				var request = "username=\""+self.username+"\",realm=\""+self.realm+"\",nonce=\""+nonce+"\",cnonce=\""+cnonce+"\",nc="+"00000001"+",qop=auth,digest-uri=\"xmpp/"+self.realm+"\",response="+self.responseHash(password)+",charset=utf-8";
				XMPP.send("<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>"+btoa(request)+"</response>");
			};
			
			/*
			 * Handle responses from the server
			 * @public
			 * @method onMessage
			 * @param {String} data XML data as a String
			 */ 
			self.onMessage = function onMessage(data){
				if(!self.jabberInit && !self.tlsDone && !self.SASL){
					if(data.indexOf("<starttls") > -1){
						self.jabberInit = true;
						XMPP.send("<starttls xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\"/>");
					}
				}else if(self.jabberInit && !self.tlsDone && !self.SASL){
					if(data.indexOf("<proceed ") > -1){
						self.tlsDone = true;
						XMPP.send("<stream:stream to='jabber.newstartrs3.com' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>");
					}
				}else if(self.jabberInit && self.tlsDone && !self.SASL){
					if(data.indexOf("<mechanism>") > -1 && data.indexOf("DIGEST-MD5") > -1){
						XMPP.send("<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='DIGEST-MD5'/>");
					}else if(data.indexOf("<challenge") > -1){
						var challenge = data.substring(0,data.indexOf("</challenge")),
						    challengeData = "";
						
						challenge = challenge.substring("<challenge".length);
						challenge = challenge.substring(challenge.indexOf(">")+1);
						//console.log("Challenge", challenge);
						challengeData = atob(challenge);
						//console.log("Decoded",challengeData);
						self.realm = challengeData.substring(challengeData.indexOf("realm=")+"realm=".length);
						self.realm = self.realm.substring(self.realm.indexOf("\"")+1);
						self.realm = self.realm.substring(0,self.realm.indexOf("\""));
						//console.log("realm",self.realm);
						nonce = challengeData.substring(challengeData.indexOf("nonce=")+"nonce=".length);
						nonce = nonce.substring(nonce.indexOf("\"")+1);
						nonce = nonce.substring(0,nonce.indexOf("\""));
						//console.log("nonce",nonce);
					}else if(data.indexOf("<success") > -1){
						self.SASL = true;
						self.complete = true;
						XMPP.send("<?xml version=\"1.0\"?><stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" xmlns=\"jabber:client\" to=\"jabber.newstartrs3.com\" version=\"1.0\">");
					}
				}
			};
			return self;
		}else{
			return null;
		}
	};
})();
