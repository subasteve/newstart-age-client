/*
 * # Jabber Client
 * @package Jabber.jabber
 * @module Client
 *
 * @description 
 * This client is used for communication to   
 * `jabber.newstartrs3.com` over port `5000`  
 * @howto
 * [XMPP protocol](http://xmpp.org/rfcs/rfc3921.html)
 */
(function(){
	/*
	 * Setup namespaces  
	 * *Only allow one instance*
	 */
	var Jabber = Jabber || {};
	Jabber.jabber = Jabber.jabber || {};
	Jabber.jabber.Client = {};
	var initilized = false;

	/*
	 * Constructor of Client   
	 * *Only one instance of this class allowed*
	 * @public
	 * @constructor
	 * @method init
	 * @param {NewstartAge.io.websocket.XMPP} XMPP Websocket to jabber2ws server
	 * @example 
	 * ```js
	 * var client = Jabber.jabber.Client.init(XMPP);
	 * ```
	 * @return {Client}
	 */
	Jabber.jabber.Client.init = function(XMPP){
		if(!initilized){
			initilized = true;
			var self = this;
			/*
			 * Session created
			 * @public
			 * @property session
			 * @return {boolean}
			 */ 
			self.session = false;
			/*
			 * Stream binded
			 * @public
			 * @property binded
			 * @return {boolean}
			 */
			self.binded = false;
			/*
			 * Online status sent
			 * @public
			 * @property presenseSent
			 * @return {boolean}
			 */
			self.presenseSent = false;
			/*
			 * Jabber ID of member
			 * @public
			 * @property jid
			 * @return {String}
			 */ 
			self.jid = "";
			/*
			 * @public
			 * @method onMessage
			 * @param {String} data XML String of data
			 */ 
			self.onMessage = function(data){
				/*
				 * ### Init procedure 
				 * * Bind to stream -> Create session  
				 * * Request friends and blocks
				 * * Set status to online
				 */ 
				if(!self.binded && !self.session){
					if(data.indexOf("<stream:features") > -1 && data.indexOf("<bind") > -1){
						XMPP.send("<iq type='set' id='bind_1'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/></iq>");
					}else if(data.indexOf("bind_1") > -1 && data.indexOf("<jid>") > -1 && data.indexOf("result") > -1){
						self.binded = true;
						self.jid = data.substring(data.indexOf("<jid>")+"<jid>".length,data.indexOf("</jid>"));
						XMPP.send("<iq type='set' id='session_1'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>");
					}
				}else if(self.binded && !self.session){
					if(data.indexOf("session_1") > -1 && data.indexOf("result") > -1){
						self.session = true;	
						//XMPP.send("<iq type='get' id='disco_1' to='jabber.newstartrs3.com'><query xmlns='http://jabber.org/protocol/disco#info'/></iq>");
						XMPP.send("<iq type='get' id='roster_1'><query xmlns='jabber:iq:roster'/></iq><iq type='get' id='blocks_1'><blocklist xmlns='urn:xmpp:blocking'/></iq>");	
					}
				}else if(self.binded && self.session){
					if(!self.presenseSent){
						self.presenseSent = true;
						XMPP.send("<presence></presence>");
					}
					//Friends list response
					if(data.indexOf("id='roster_1'") > -1){
						data = data.substring(data.indexOf("<query xmlns='jabber:iq:roster'>")+"<query xmlns='jabber:iq:roster'>".length,data.indexOf("</query>"));
						m.startComputation();
						while(data.indexOf("jid='") > -1){
							data = data.substring(data.indexOf("jid='")+"jid='".length);
							Jabber.Views.Main.viewModelInstance.friends.push(_.defaults({
								name: m.prop(data.substring(0,data.indexOf("@jabber.newstartrs3.com")))
							},Jabber.Views.Main.viewModelInstance.friendSchema));
						}
						m.endComputation();
					}else if(data.indexOf("<presence xmlns='jabber:client'") > -1){
						var friendName = data.substring(data.indexOf("from='")+"from='".length),
						status = "",
						statusMessage = "",
						client = "",
						voice = false,
						camera = false,
						video = false,
						found = false,
						online = true;
						friendName = friendName.substring(0,friendName.indexOf("@jabber.newstartrs3.com/"));
						if(data.indexOf("<show>") > -1 && data.indexOf("</show>") > -1){
							status = data.substring(data.indexOf("<show>")+"<show>".length,data.indexOf("</show>"));
						}
						if(data.indexOf("<status>") > -1 && data.indexOf("</status>") > -1){
							statusMessage = data.substring(data.indexOf("<status>")+"<status>".length,data.indexOf("</status>"));
						}
						if(data.indexOf("ext='") > -1){
							var extStr = data.substring(data.indexOf("ext='")+"ext='".length);
							extStr = extStr.substring(data.indexOf("'"));
							if(extStr.indexOf("voice") > -1){
								voice = true;
							}
							if(extStr.indexOf("camera") > -1){
								camera = true;
							}
							if(extStr.indexOf("video") > -1){
								video = true;
							}
						}
						if(data.indexOf("node='") > -1){
							var clientStr = data.substring(data.indexOf("node='")+"node='".length);
							clientStr = clientStr.substring(0,data.indexOf("'"));		
							if(clientStr === "http://pidgin.im/"){
								client = "Pidgin";
							}
						}
						if(data.indexOf("Smack") > -1){
							client = "Smack";
						}
						if(data.indexOf("type='") > -1){
							var typeStr = data.substring(data.indexOf("type='")+"type='".length);
							typeStr = typeStr.substring(0,typeStr.indexOf("'"));
							if(typeStr === "unavailable"){
								online = false;
							}
						}
						m.startComputation();
						for(var i = 0; i < Jabber.Views.Main.viewModelInstance.friends.length; i++){
							if(Jabber.Views.Main.viewModelInstance.friends[i].name() === friendName){
								found = true;
								Jabber.Views.Main.viewModelInstance.friends[i].online(online);
								if(status === ""){
									Jabber.Views.Main.viewModelInstance.friends[i].idle(false);
									Jabber.Views.Main.viewModelInstance.friends[i].busy(false);
								}else if(status === "away"){
									Jabber.Views.Main.viewModelInstance.friends[i].idle(true);
									Jabber.Views.Main.viewModelInstance.friends[i].busy(false);
								}else if(status === "busy" || status === "dnd"){
									Jabber.Views.Main.viewModelInstance.friends[i].idle(false);
									Jabber.Views.Main.viewModelInstance.friends[i].busy(true);
								}
								Jabber.Views.Main.viewModelInstance.friends[i].status(statusMessage);
								Jabber.Views.Main.viewModelInstance.friends[i].voice(voice);
								Jabber.Views.Main.viewModelInstance.friends[i].camera(camera);
								Jabber.Views.Main.viewModelInstance.friends[i].video(video);
								break;

							}
						}
						if(!found){
							if(Jabber.Views.Main.viewModelInstance.username() !== friendName){
								Jabber.Views.Main.viewModelInstance.friends.push(_.defaults({
									name: m.prop(friendName),
									online: m.prop(online),
									idle: (status === "away") ? m.prop(true) : m.prop(false),
									busy: (status === "busy" || status === "dnd") ? m.prop(true) : m.prop(false),
									status: m.prop(statusMessage),
									voice: m.prop(voice),
									camera: m.prop(camera),
									video: m.prop(video)	
								},Jabber.Views.Main.viewModelInstance.friendSchema));
							}else{
								//Handle multi clients logged in
								console.log(data);
							}
						}
						m.endComputation();
					}else{
						console.log(data);
					}
				}
			};
			return self;
		}else{
			return null;
		}
	};

})();
