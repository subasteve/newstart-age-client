/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 */

(function(){
	NewstartAge.encryption = NewstartAge.encryption || {};
	NewstartAge.encryption.MD5 = {
		hexcase: false, /* hex output format. false - lowercase; true - uppercase        */
		b64pad: "", /* base-64 pad character. "=" for strict RFC compliance   */
		chrsz: 8,
		/*
		 * Convert an array of big-endian words to a base-64 string
		 */
		binb2b64: function(binarray){
			 var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
			     str = "",
			     i,
			     triplet,
			     j;	     
			 for (i = 0; i < binarray.length * 4; i += 3) {
			 	 triplet = (((binarray[i >> 2] >> 8 * (3 - i % 4)) & 0xFF) << 16) | (((binarray[i + 1 >> 2] >> 8 * (3 - (i + 1) % 4)) & 0xFF) << 8) | ((binarray[i + 2 >> 2] >> 8 * (3 - (i + 2) % 4)) & 0xFF);
			 	 for (j = 0; j < 4; j++) {
			 	 	 if (i * 8 + j * 6 > binarray.length * 32){
			 	 	 	 str += Jabber.encryption.MD5.b64pad;
					 }else{
					 	 str += tab.charAt((triplet >> 6 * (3 - j)) & 0x3F);
					 }
				 }
			 }
			 return str;
		},
		/*
		 * Convert an 8-bit or 16-bit string to an array of big-endian words
		 * In 8-bit function, characters >255 have their hi-byte silently ignored.
		 */
		str2binb: function(str){
			 var bin = [],
			     mask = (1 << Jabber.encryption.MD5.chrsz) - 1;
			 for (var i = 0; i < str.length * Jabber.encryption.MD5.chrsz; i += Jabber.encryption.MD5.chrsz){
			 	 bin[i >> 5] |= (str.charCodeAt(i / Jabber.encryption.MD5.chrsz) & mask) << (24 - i % 32);
			 }
			 return bin;
		},
		/*
 		* Convert a raw string to an array of little-endian words
 		* Characters >255 have their high-byte silently ignored.
 		*/
		rstr2binl: function(input){
			var output = new Array(input.length >> 2), 
      			    i;
  			for(i = 0; i < output.length; i++){
    				output[i] = 0;
  			}
  			for(i = 0; i < input.length * 8; i += 8){
    				output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (i%32);
  			}
  			return output;
		},
		/*
 		* Encode a string as utf-16
 		*/
		str2rstrUtf16le: function(input){
			var output = "",
      			    i;	
  			for(i = 0; i < input.length; i++){
    				output += String.fromCharCode( input.charCodeAt(i)        & 0xFF,
                                  				(input.charCodeAt(i) >>> 8) & 0xFF);
  			}
  			return output;
		},
		str2rstrUtf16be: function(input){
			var output = "",
      		    	    i;
  			for(i = 0; i < input.length; i++){
    				output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
                                   				input.charCodeAt(i)        & 0xFF);
  			}
  			return output;
		},
 		/*
 		* Encode a string as utf-8.
 		* For efficiency, this assumes the input is valid utf-16.
 		*/
		str2rstrUtf8: function(input){
			var output = "",
      			    i = -1,
      			    x,
      			    y;	      

  			while(++i < input.length){
    				/* Decode utf-16 surrogate pairs */
    				x = input.charCodeAt(i);
    				y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
    				if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF){
      					x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
      					i++;
    				}

    				/* Encode output as utf-8 */
    				if(x <= 0x7F){
      					output += String.fromCharCode(x);
    				}else if(x <= 0x7FF){
      					output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
                        	            				0x80 | ( x         & 0x3F));
    				}else if(x <= 0xFFFF){
      					output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                        				            0x80 | ((x >>> 6 ) & 0x3F),
                        	        			    0x80 | ( x         & 0x3F));
    				}else if(x <= 0x1FFFFF){
      					output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                        	        				0x80 | ((x >>> 12) & 0x3F),
                        	            				0x80 | ((x >>> 6 ) & 0x3F),
                        	            				0x80 | ( x         & 0x3F));
    				}
  			}
  			return output;
		},
 		/*
 		* Convert a raw string to an arbitrary string encoding
 		*/
		rstr2any: function(input, encoding){
			var divisor = encoding.length,
      			    i,
      			    j,
      			    q,
      			    x,
      			    quotient,
      			    /* Convert to an array of 16-bit big-endian values, forming the dividend */
      			    dividend = new Array(Math.ceil(input.length / 2)),
      			    /*
   			    * Repeatedly perform a long division. The binary array forms the dividend,
   			    * the length of the encoding is the divisor. Once computed, the quotient
   			    * forms the dividend for the next step. All remainders are stored for later
   			    * use.
   			    */
      			    full_length = Math.ceil(input.length * 8 / (Math.log(encoding.length) / Math.log(2))),
      			    remainders = new Array(full_length),
      			    output = "";

  			for(i = 0; i < dividend.length; i++){
    				dividend[i] = (input.charCodeAt(i * 2) << 8) | input.charCodeAt(i * 2 + 1);
  			}

  			for(j = 0; j < full_length; j++){
    				quotient = [];
    				x = 0;
    				for(i = 0; i < dividend.length; i++){
     			 		x = (x << 16) + dividend[i];
      					q = Math.floor(x / divisor);
      					x -= q * divisor;
      					if(quotient.length > 0 || q > 0){
        					quotient[quotient.length] = q;
      					}
    				}
    				remainders[j] = x;
    				dividend = quotient;
  			}

  			/* Convert the remainders to the output string */
  			for(i = remainders.length - 1; i >= 0; i--){
    				output += encoding.charAt(remainders[i]);
  			}

  			return output;
		},
 		/*
 		* Convert a raw string to a base-64 string
 		*/
 		rstr2b64: function(input){
			var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
      			    output = "",
      			    len = input.length,
      			    j = 0,
      			    i = 0,
      			    triplet;
  
  			for(i = 0; i < len; i += 3){
 
    				triplet = (input.charCodeAt(i) << 16)
                			| (i + 1 < len ? input.charCodeAt(i+1) << 8 : 0)
                			| (i + 2 < len ? input.charCodeAt(i+2)      : 0);
    				for(j = 0; j < 4; j++){
   
      					if(i * 8 + j * 6 > input.length * 8){
      						output += Jabber.encryption.MD5.b64pad;
      					}else{
      						output += tab.charAt((triplet >>> 6*(3-j)) & 0x3F);
      					}
    				}
  			}
  			return output;
		},
 		/*
 		* Convert a raw string to a hex string
 		*/
		rstr2hex: function(input){
			var hex_tab = Jabber.encryption.MD5.hexcase ? "0123456789ABCDEF" : "0123456789abcdef",
      			    output = "",
      			    x,
      			    i = 0;
  
  			for(i = 0; i < input.length; i++){
    				x = input.charCodeAt(i);
    				output += hex_tab.charAt((x >>> 4) & 0x0F) + hex_tab.charAt(x & 0x0F);
  			}
  			return output;
		},
		/*
 		* Convert an array of little-endian words to a string
 		*/
		binl2rstr: function(input){
			var output = "",
      			i;
  			for(i = 0; i < input.length * 32; i += 8){
    				output += String.fromCharCode((input[i>>5] >>> (i % 32)) & 0xFF);
  			}
  			return output;
		},
 		/*
 		* Add integers, wrapping at 2^32. This uses 16-bit operations internally
 		* to work around bugs in some JS interpreters.
 		*/
 		safeAdd: function(x,y){
			var lsw = (x & 0xFFFF) + (y & 0xFFFF),
			    msw = (x >> 16) + (y >> 16) + (lsw >> 16);
			return (msw << 16) | (lsw & 0xFFFF);
		},
		/*
 		* Bitwise rotate a 32-bit number to the left.
 		*/
		bitRol: function(num,cnt){
			return (num << cnt) | (num >>> (32 - cnt));
		},
		/*
		 * These functions implement the four basic operations the algorithm uses.
		 */
		md5Cmn: function(q,a,b,x,s,t){
			return Jabber.encryption.MD5.safeAdd(Jabber.encryption.MD5.bitRol(Jabber.encryption.MD5.safeAdd(Jabber.encryption.MD5.safeAdd(a, q), Jabber.encryption.MD5.safeAdd(x, t)), s),b);
		},
		md5Ff: function(a,b,c,d,x,s,t){
			return Jabber.encryption.MD5.md5Cmn((b & c) | ((~b) & d), a, b, x, s, t);
		},
		md5Gg: function(a,b,c,d,x,s,t){
			return Jabber.encryption.MD5.md5Cmn((b & d) | (c & (~d)), a, b, x, s, t);
		},
		md5Hh: function(a,b,c,d,x,s,t){
			return Jabber.encryption.MD5.md5Cmn(b ^ c ^ d, a, b, x, s, t);	
		},
		md5Ii: function(a,b,c,d,x,s,t){
			return Jabber.encryption.MD5.md5Cmn(c ^ (b | (~d)), a, b, x, s, t);	
		},
		/*
 		* Calculate the MD5 of an array of little-endian words, and a bit length.
 		*/
		binlMd5: function(x, len){
			/* append padding */
  			x[len >> 5] |= 0x80 << ((len) % 32);
  			x[(((len + 64) >>> 9) << 4) + 14] = len;

  			var a =  1732584193,
  			    b = -271733879,
  			    c = -1732584194,
  			    d =  271733878,
  			    olda,
  			    oldb,
  			    oldc,
  			    oldd,
  			    i;

  			for(i = 0; i < x.length; i += 16){
    				olda = a;
    				oldb = b;
    				oldc = c;
    				oldd = d;

    				a = Jabber.encryption.MD5.md5Ff(a, b, c, d, x[i+ 0], 7 , -680876936);
    				d = Jabber.encryption.MD5.md5Ff(d, a, b, c, x[i+ 1], 12, -389564586);
    				c = Jabber.encryption.MD5.md5Ff(c, d, a, b, x[i+ 2], 17,  606105819);
    				b = Jabber.encryption.MD5.md5Ff(b, c, d, a, x[i+ 3], 22, -1044525330);
    				a = Jabber.encryption.MD5.md5Ff(a, b, c, d, x[i+ 4], 7 , -176418897);
    				d = Jabber.encryption.MD5.md5Ff(d, a, b, c, x[i+ 5], 12,  1200080426);
    				c = Jabber.encryption.MD5.md5Ff(c, d, a, b, x[i+ 6], 17, -1473231341);
    				b = Jabber.encryption.MD5.md5Ff(b, c, d, a, x[i+ 7], 22, -45705983);
    				a = Jabber.encryption.MD5.md5Ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
    				d = Jabber.encryption.MD5.md5Ff(d, a, b, c, x[i+ 9], 12, -1958414417);
    				c = Jabber.encryption.MD5.md5Ff(c, d, a, b, x[i+10], 17, -42063);
    				b = Jabber.encryption.MD5.md5Ff(b, c, d, a, x[i+11], 22, -1990404162);
    				a = Jabber.encryption.MD5.md5Ff(a, b, c, d, x[i+12], 7 ,  1804603682);
    				d = Jabber.encryption.MD5.md5Ff(d, a, b, c, x[i+13], 12, -40341101);
    				c = Jabber.encryption.MD5.md5Ff(c, d, a, b, x[i+14], 17, -1502002290);
    				b = Jabber.encryption.MD5.md5Ff(b, c, d, a, x[i+15], 22,  1236535329);

    				a = Jabber.encryption.MD5.md5Gg(a, b, c, d, x[i+ 1], 5 , -165796510);
    				d = Jabber.encryption.MD5.md5Gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
    				c = Jabber.encryption.MD5.md5Gg(c, d, a, b, x[i+11], 14,  643717713);
    				b = Jabber.encryption.MD5.md5Gg(b, c, d, a, x[i+ 0], 20, -373897302);
    				a = Jabber.encryption.MD5.md5Gg(a, b, c, d, x[i+ 5], 5 , -701558691);
    				d = Jabber.encryption.MD5.md5Gg(d, a, b, c, x[i+10], 9 ,  38016083);
    				c = Jabber.encryption.MD5.md5Gg(c, d, a, b, x[i+15], 14, -660478335);
    				b = Jabber.encryption.MD5.md5Gg(b, c, d, a, x[i+ 4], 20, -405537848);
    				a = Jabber.encryption.MD5.md5Gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
    				d = Jabber.encryption.MD5.md5Gg(d, a, b, c, x[i+14], 9 , -1019803690);
    				c = Jabber.encryption.MD5.md5Gg(c, d, a, b, x[i+ 3], 14, -187363961);
    				b = Jabber.encryption.MD5.md5Gg(b, c, d, a, x[i+ 8], 20,  1163531501);
    				a = Jabber.encryption.MD5.md5Gg(a, b, c, d, x[i+13], 5 , -1444681467);
    				d = Jabber.encryption.MD5.md5Gg(d, a, b, c, x[i+ 2], 9 , -51403784);
    				c = Jabber.encryption.MD5.md5Gg(c, d, a, b, x[i+ 7], 14,  1735328473);
    				b = Jabber.encryption.MD5.md5Gg(b, c, d, a, x[i+12], 20, -1926607734);

    				a = Jabber.encryption.MD5.md5Hh(a, b, c, d, x[i+ 5], 4 , -378558);
    				d = Jabber.encryption.MD5.md5Hh(d, a, b, c, x[i+ 8], 11, -2022574463);
    				c = Jabber.encryption.MD5.md5Hh(c, d, a, b, x[i+11], 16,  1839030562);
    				b = Jabber.encryption.MD5.md5Hh(b, c, d, a, x[i+14], 23, -35309556);
    				a = Jabber.encryption.MD5.md5Hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
    				d = Jabber.encryption.MD5.md5Hh(d, a, b, c, x[i+ 4], 11,  1272893353);
    				c = Jabber.encryption.MD5.md5Hh(c, d, a, b, x[i+ 7], 16, -155497632);
    				b = Jabber.encryption.MD5.md5Hh(b, c, d, a, x[i+10], 23, -1094730640);
    				a = Jabber.encryption.MD5.md5Hh(a, b, c, d, x[i+13], 4 ,  681279174);
    				d = Jabber.encryption.MD5.md5Hh(d, a, b, c, x[i+ 0], 11, -358537222);
    				c = Jabber.encryption.MD5.md5Hh(c, d, a, b, x[i+ 3], 16, -722521979);
    				b = Jabber.encryption.MD5.md5Hh(b, c, d, a, x[i+ 6], 23,  76029189);
    				a = Jabber.encryption.MD5.md5Hh(a, b, c, d, x[i+ 9], 4 , -640364487);
    				d = Jabber.encryption.MD5.md5Hh(d, a, b, c, x[i+12], 11, -421815835);
    				c = Jabber.encryption.MD5.md5Hh(c, d, a, b, x[i+15], 16,  530742520);
    				b = Jabber.encryption.MD5.md5Hh(b, c, d, a, x[i+ 2], 23, -995338651);

    				a = Jabber.encryption.MD5.md5Ii(a, b, c, d, x[i+ 0], 6 , -198630844);
    				d = Jabber.encryption.MD5.md5Ii(d, a, b, c, x[i+ 7], 10,  1126891415);
    				c = Jabber.encryption.MD5.md5Ii(c, d, a, b, x[i+14], 15, -1416354905);
    				b = Jabber.encryption.MD5.md5Ii(b, c, d, a, x[i+ 5], 21, -57434055);
    				a = Jabber.encryption.MD5.md5Ii(a, b, c, d, x[i+12], 6 ,  1700485571);
    				d = Jabber.encryption.MD5.md5Ii(d, a, b, c, x[i+ 3], 10, -1894986606);
    				c = Jabber.encryption.MD5.md5Ii(c, d, a, b, x[i+10], 15, -1051523);
    				b = Jabber.encryption.MD5.md5Ii(b, c, d, a, x[i+ 1], 21, -2054922799);
    				a = Jabber.encryption.MD5.md5Ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
    				d = Jabber.encryption.MD5.md5Ii(d, a, b, c, x[i+15], 10, -30611744);
    				c = Jabber.encryption.MD5.md5Ii(c, d, a, b, x[i+ 6], 15, -1560198380);
    				b = Jabber.encryption.MD5.md5Ii(b, c, d, a, x[i+13], 21,  1309151649);
    				a = Jabber.encryption.MD5.md5Ii(a, b, c, d, x[i+ 4], 6 , -145523070);
    				d = Jabber.encryption.MD5.md5Ii(d, a, b, c, x[i+11], 10, -1120210379);
    				c = Jabber.encryption.MD5.md5Ii(c, d, a, b, x[i+ 2], 15,  718787259);
    				b = Jabber.encryption.MD5.md5Ii(b, c, d, a, x[i+ 9], 21, -343485551);

    				a = Jabber.encryption.MD5.safeAdd(a, olda);
    				b = Jabber.encryption.MD5.safeAdd(b, oldb);
    				c = Jabber.encryption.MD5.safeAdd(c, oldc);
    				d = Jabber.encryption.MD5.safeAdd(d, oldd);
  			}
  			return [a, b, c, d];
		},
		/*
 		* Calculate the MD5 of a raw string
 		*/
		rstrMd5: function(s){
			return Jabber.encryption.MD5.binl2rstr(Jabber.encryption.MD5.binlMd5(Jabber.encryption.MD5.rstr2binl(s), s.length * 8));
		},
		/*
 		* Calculate the HMAC-MD5, of a key and some data (raw strings)
 		*/
		rstrHmacMd5: function(key,data){
			var bkey = Jabber.encryption.MD5.rstr2binl(key),
      			    ipad = new Array(16),
      			    opad = new Array(16),
      			    hash = "",
      			    i = 0;
  
  			if(bkey.length > 16) {
  	  			bkey = Jabber.encryption.MD5.binlMd5(bkey, key.length * 8);
  			}

  			for(i = 0; i < 16; i++){
    				ipad[i] = bkey[i] ^ 0x36363636;
    				opad[i] = bkey[i] ^ 0x5C5C5C5C;
  			}

  			hash = Jabber.encryption.MD5.binlMd5(ipad.concat(Jabber.encryption.MD5.rstr2binl(data)), 512 + data.length * 8);
  			return Jabber.encryption.MD5.binl2rstr(Jabber.encryption.MD5.binlMd5(opad.concat(hash), 512 + 128));
		},
		/*
 		* These are the functions you'll usually want to call
 		* They take string arguments and return either hex or base-64 encoded strings
 		*/
		hexMd5: function(s){
			return Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrMd5(Jabber.encryption.MD5.str2rstrUtf8(s)));
		},
		b64Md5: function(s){
			return Jabber.encryption.MD5.rstr2b64(Jabber.encryption.MD5.rstrMd5(Jabber.encryption.MD5.str2rstrUtf8(s)));
		},
		anyMd5: function(s,e){
			return Jabber.encryption.MD5.rstr2any(Jabber.encryption.MD5.rstrMd5(Jabber.encryption.MD5.str2rstrUtf8(s)), e);
		},
		hexHmacMd5: function(k,d){
			return Jabber.encryption.MD5.rstr2hex(Jabber.encryption.MD5.rstrHmacMd5(Jabber.encryption.MD5.str2rstrUtf8(k), Jabber.encryption.MD5.str2rstrUtf8(d)));
		},
		b64HmacMd5: function(k,d){
			return Jabber.encryption.MD5.rstr2b64(Jabber.encryption.MD5.rstrHmacMd5(Jabber.encryption.MD5.str2rstrUtf8(k), Jabber.encryption.MD5.str2rstrUtf8(d)));
		},
		anyHmacMd5: function(k,d,e){
			return Jabber.encryption.MD5.rstr2any(Jabber.encryption.MD5.rstrHmacMd5(Jabber.encryption.MD5.str2rstrUtf8(k), Jabber.encryption.MD5.str2rstrUtf8(d)), e);
		},
		md5Test: function(){
			return Jabber.encryption.MD5.hexMd5("abc").toLowerCase() === "900150983cd24fb0d6963f7d28e17f72";
		}
	};
})();
